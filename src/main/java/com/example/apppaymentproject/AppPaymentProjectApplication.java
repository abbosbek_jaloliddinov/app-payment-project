package com.example.apppaymentproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppPaymentProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppPaymentProjectApplication.class, args);
    }

}
