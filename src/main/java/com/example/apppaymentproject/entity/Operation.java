package com.example.apppaymentproject.entity;

import com.example.apppaymentproject.base.BaseEntity;
import com.example.apppaymentproject.enums.CurrencyType;
import com.example.apppaymentproject.enums.OperationStatus;
import com.example.apppaymentproject.enums.OperationType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "operations")
public class Operation extends BaseEntity {

    @Enumerated(value = EnumType.STRING)
    @Column(name = "operation_status", length = 10)
    private OperationStatus operationStatus;

    @Column(name = "amount")
    private Float amount;

    @Column(name = "commission")
    private Float commission;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "currency_type", length = 10)
    private CurrencyType currencyType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    private User user;
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "sender_masked_pan", length = 16)
    private String senderMaskedPan;

    @Column(name = "sender_pan", length = 16)
    private String senderPan;

    @Column(name = "sender_token")
    private String senderToken;

    @Column(name = "sender_merchant")
    private String senderMerchant;

    @Column(name = "sender_terminal")
    private String senderTerminal;

    @Column(name = "receiver_masked_pan", length = 16)
    private String receiverMaskedPan;

    @Column(name = "receiver_pan", length = 16)
    private String receiverPan;

    @Column(name = "receiver_token")
    private String receiverToken;

    @Column(name = "receiver_merchant")
    private String receiverMerchant;

    @Column(name = "receiver_terminal")
    private String receiverTerminal;

    @Column(name = "operation_code", length = 15)
    private String operationCode;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "operation_type", length = 10)
    private OperationType operationType;


    @Column(name = "start_time", columnDefinition = "TIMESTAMP DEFAULT NOW()")
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime startTime = LocalDateTime.now();

    @Column(name = "end_time", columnDefinition = "TIMESTAMP DEFAULT NULL")
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime endTime;

    @Column(name = "confirm_code")
    private Integer confirmCode;

}
