package com.example.apppaymentproject.entity;

import com.example.apppaymentproject.base.BaseEntity;
import com.example.apppaymentproject.enums.CardType;
import com.example.apppaymentproject.enums.CurrencyType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "cards")
@AllArgsConstructor
@NoArgsConstructor
public class Card extends BaseEntity {

    @Column(name = "card_name")
    private String cardName;

    @Column(name = "pan",unique = true,length = 16)
    private String pan;

    @Column(name = "expiry",length = 10)
    private String expiry;

    @Column(name = "masked_pan",unique = true,length = 16)
    private String maskedPan;

    @Column(name = "card_holder_name")
    private String cardHolderName;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "card_type")
    private CardType cardType;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "currency_type")
    private CurrencyType currencyType;

    @Column(name = "phone",length = 15)
    private String phone;

    @Column(name = "balance")
    private Double balance;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id",insertable = false,updatable = false)
    private User user;
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "card_token")
    private String carToken;
}
