package com.example.apppaymentproject.entity;

import com.example.apppaymentproject.base.BaseEntity;
import com.example.apppaymentproject.enums.TransactionStatus;
import com.example.apppaymentproject.enums.TransactionType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transactions")
public class Transaction extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operation_id", referencedColumnName = "id",insertable = false, updatable = false)
    private Operation operation;
    @Column(name = "operation_id")
    private Long operationId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "transaction_type",length = 15)
    private TransactionType transactionType;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "transaction_status",length = 15)
    private TransactionStatus transactionStatus;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "response_id")
    private String responseId;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "amount")
    private Float amount;



}
