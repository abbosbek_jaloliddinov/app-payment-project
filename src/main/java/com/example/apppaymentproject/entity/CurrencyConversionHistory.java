package com.example.apppaymentproject.entity;

import com.example.apppaymentproject.enums.CurrencyType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "currency_conversion_history")
public class CurrencyConversionHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "from_currency_type",length = 10)
    private CurrencyType fromCurrencyType;

    // input amount in from currency type
    @Column(name = "first_amount")
    private Float firstAmount;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "to_currency_type",length = 10)
    private CurrencyType toCurrencyType;

    //converted amount
    @Column(name = "converted_amount")
    private Float convertedAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id",insertable = false,updatable = false)
    private User user;
    @Column(name = "user_id")
    private Long userId;
}
