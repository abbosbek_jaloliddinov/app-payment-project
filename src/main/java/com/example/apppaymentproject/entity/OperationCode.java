package com.example.apppaymentproject.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "operation_code")
public class OperationCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code",unique = true,nullable = false)
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "commission_rate")
    private Integer commissionRate;
}
