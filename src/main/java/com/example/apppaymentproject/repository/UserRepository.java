package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User> {
}
