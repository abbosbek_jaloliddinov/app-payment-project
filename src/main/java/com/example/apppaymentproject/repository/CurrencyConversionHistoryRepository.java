package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.CurrencyConversionHistory;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyConversionHistoryRepository extends BaseRepository<CurrencyConversionHistory> {
}
