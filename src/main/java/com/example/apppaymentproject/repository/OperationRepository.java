package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.Operation;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationRepository extends BaseRepository<Operation> {
}
