package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends BaseRepository<Role> {
}
