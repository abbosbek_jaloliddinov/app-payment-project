package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.Wallet;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends BaseRepository<Wallet> {
}
