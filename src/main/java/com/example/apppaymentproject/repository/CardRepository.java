package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.Card;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends BaseRepository<Card> {
}
