package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.Notification;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends BaseRepository<Notification> {
}
