package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.CurrencyRate;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRateRepository extends BaseRepository<CurrencyRate> {
}
