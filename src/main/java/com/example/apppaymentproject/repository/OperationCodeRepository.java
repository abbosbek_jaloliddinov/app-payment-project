package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.OperationCode;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationCodeRepository extends BaseRepository<OperationCode> {
}
