package com.example.apppaymentproject.repository;

import com.example.apppaymentproject.entity.Transaction;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends BaseRepository<Transaction> {
}
