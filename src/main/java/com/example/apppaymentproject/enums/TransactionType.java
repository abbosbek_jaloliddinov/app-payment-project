package com.example.apppaymentproject.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum TransactionType {

    DEBIT("receive money"),
    CREDIT("withdraw money"),
    UNKNOWN("unknown");

    private final String description;

    public static TransactionType getByName(final String name) {
        return Arrays.stream(TransactionType.values())
                .filter(transactionType-> transactionType.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }

}
