package com.example.apppaymentproject.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum OperationType {

    P2P("card to card"),
    P2S("card to service"),
    UNKNOWN("unknown");

    private final String description;


    public static OperationType getByName(final String name) {
        return Arrays.stream(OperationType.values())
                .filter(operationType-> operationType.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }
}
