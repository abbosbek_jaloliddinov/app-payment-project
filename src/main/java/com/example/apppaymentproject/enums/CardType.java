package com.example.apppaymentproject.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum CardType {
    UZCARD("UZCARD"),
    HUMO("HUMO"),
    VISA("VISA"),
    MASTER("MASTER"),
    WALLET("WALLET"),
    UNKNOWN("UNKNOWN");


    private final String fullName;

    public static CardType getByName(final String name) {
        return Arrays.stream(CardType.values())
                .filter(cardType-> cardType.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }

}
