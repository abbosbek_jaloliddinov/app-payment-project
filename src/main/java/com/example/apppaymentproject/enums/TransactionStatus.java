package com.example.apppaymentproject.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum TransactionStatus {

    PENDING,
    SUCCESS,
    ERROR,
    UNKNOWN;

    public static TransactionStatus getByName(final String name) {
        return Arrays.stream(TransactionStatus.values())
                .filter(transactionStatus-> transactionStatus.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }

}
