package com.example.apppaymentproject.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum OperationStatus {

    HOLD("beginning of transfer"),
    PENDING("withdrawing"),
    SUCCESS("withdrawn successfully"),
    FAILED("transfer failed"),
    REVERSE("reversed"),
    UNKNOWN("unknown transfer");

    private final String description;


    public static OperationStatus getByName(final String name) {
        return Arrays.stream(OperationStatus.values())
                .filter(operationStatus-> operationStatus.name().equalsIgnoreCase(name))
                .findFirst()
                .orElse(UNKNOWN);
    }
}
